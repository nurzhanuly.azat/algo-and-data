import java.util.Scanner;

public class PrimeTest{
	
	public static String checkPrime(int number){
		int counter = 0;
		String result;
		//divisibility test
		for(int i =1; i < number;i++){
			if(number%i==0) counter++;
		}
		return result = (counter < 2) ? "Prime" : "Composite";
	}

	public static void main(String[] args){
		Scanner cInt = new Scanner(System.in);
		int someNumber;
		someNumber = cInt.nextInt();
		System.out.println(checkPrime(someNumber));
	}
}