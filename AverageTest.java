import java.util.Scanner;
import java.util.Arrays;

public class AverageTest{

	static int returnAverage(int age){
		int[] userArray = new int[age];
		int average = 0;
		//Created scan for elements 
		Scanner userScan = new Scanner(System.in);
		System.out.println("Elements: ");
		//Enter the elements
		for(int i = 0; i < age;i++){
			userArray[i] = userScan.nextInt();
		}
		//finding average
		for(int i = 0; i < age;i++){
			average += userArray[i];
		}
		
		//return userArray[0];
		return average/age;
	}

	public static void main(String[] args){
				int userInput;
				Scanner userScan = new Scanner(System.in);
				//user size input
				System.out.println("Size: ");
				userInput = userScan.nextInt();
				System.out.println(returnAverage(userInput));
	}
}