import java.util.Scanner;
class BinomTest{
    static int result ( int n , int k){
         int binom[][] = new int[n+1][k+1];
         int i;
         int j;
         int check;
         for (i=0 ; i<=n ;i++){
             check = i>k ? k : i;

             for(j=0 ; j<=check ;j++){
                  if(i==0 || j==0) binom[i][j]=1;
                   else
                   binom[i][j] = binom[i-1][j-1] + binom[i-1][j];
              }
         }
         
         return binom[n][k];
     }
    
    
    
    public static void main ( String args[])
     {
         Scanner scan = new Scanner ( System.in) ;
         
           int n = scan.nextInt();
           int k = scan.nextInt();
           
           System.out.println(result(n,k));
     }
    
}